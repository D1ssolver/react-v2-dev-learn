import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

function NavbarComponent() {
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Navbar.Brand href="/">Магазин</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Главная</Nav.Link>
          <Nav.Link href="/categories">Категории</Nav.Link>
          <Nav.Link href="/cart">Корзина</Nav.Link>
        </Nav>
        <Nav>
          <NavDropdown title="Профиль" id="basic-nav-dropdown">
            <NavDropdown.Item href="/registration">Регистрация</NavDropdown.Item>
            <NavDropdown.Item href="/login">Войти</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/logout">Выйти</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavbarComponent;