import React, { useState } from 'react';
import { Card, Button } from 'react-bootstrap';

function ProductCard({ product, onAddToCart }) {
  const { id, name, price, image } = product;

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={image} alt={name} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{price} руб.</Card.Text>
        <Button variant="primary" className="btn-block" onClick={() => onAddToCart(product)}>
          Добавить в корзину
        </Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;



