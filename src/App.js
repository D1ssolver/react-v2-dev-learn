import NavbarComponent from './NavbarComponent';
import React from 'react';
import ProductCard from './ProductCard';
import Cart from './Card';
import { useState } from 'react';

export default function App() {
  return (
    <>
      <div>
        <NavbarComponent />
      </div>
      <div>
        <h1>Hello World</h1>
        <ProductList />
        <CartPage />
      </div>
    </>
  );
}


const initialCart = []; // Начальное состояние корзины

function ProductList() {
  const [cartItems, setCartItems] = useState(initialCart);

  const addToCart = product => {
    // Добавить товар в корзину
    // Может потребоваться обработка дубликатов
    setCartItems([...cartItems, product]);
  };

  return (
    <div>
      {products.map(product => (
        <ProductCard key={product.id} product={product} onAddToCart={addToCart} />
      ))}
      <Cart cartItems={cartItems} />
    </div>
  );
}



const cartItems = [
  { id: 1, name: 'Товар 1', price: 100, quantity: 2 },
  { id: 2, name: 'Товар 2', price: 150, quantity: 1 },
  // ...другие товары в корзине
];

function CartPage() {
  return (
    <div>
      <h2>Ваша корзина</h2>
      <Cart cartItems={cartItems} />
    </div>
  );
}


