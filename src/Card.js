import React from 'react';
import { Card, Button } from 'react-bootstrap';

function Cart({ cartItems }) {
  // ...

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Title>Ваша корзина</Card.Title>
        {cartItems.map(item => (
          <div key={item.id}>
            {/* ... */}
          </div>
        ))}
        <hr />
        <p>Общая сумма: {calculateTotal()} руб.</p>
        <Button variant="success" className="btn-block">
          Оформить заказ
        </Button>
      </Card.Body>
    </Card>
  );
}

export default Cart;